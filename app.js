const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = 8019;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

// Store data in-memory (for demonstration purposes, not suitable for production)
let users = [];
const userDataFilePath = 'users.json';

// Load existing users data from file if it exists
if (fs.existsSync(userDataFilePath)) {
  const userData = fs.readFileSync(userDataFilePath);
  users = JSON.parse(userData);
}

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Registration endpoint
app.post('/register', (req, res) => {
  const { username, password } = req.body;

  // Check if the username already exists
  if (users.find(user => user.username === username)) {
    return res.send('Username already exists. Choose a different username.');
  }

  const newUser = { username, password };
  users.push(newUser);
  saveUserData();
  res.send(`User ${username} registered successfully.`);
});

// Login endpoint
app.post('/login', (req, res) => {
  const { username, password } = req.body;

  // Check if the username and password match
  const user = users.find(user => user.username === username && user.password === password);

  if (user) {
    res.send(`Welcome, ${username}!`);
  } else {
    res.send('Invalid username or password.');
  }
});

app.listen(port, () => {
    console.log(`Application is listening at http://localhost:${port}`);
});


// Save user data to the file
function saveUserData() {
  fs.writeFileSync(userDataFilePath, JSON.stringify(users, null, 2));
}
